import React, { useEffect, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import ListItem from '@material-ui/core/ListItem';
import { data } from './data';

const Filters = () => {

    //sorting all the items alphabetically
    const sortedByTitle = data.items.sort((x, y) => {
        return (x.title < y.title) ? -1 : ((x.title > y.title) ? 1 : 0)
    });

    const [itemsList, setItemsList] = useState(sortedByTitle);
    const [categoryName, setCategoryName] = useState("");
    const [selectedIndex, setSelectedIndex] = useState('')
    
    //when never category changes
    const handleCategoryClick = (categoryName, index) => {
        //Deselect the category and showing all the items
        if (index === selectedIndex) {
            setCategoryName('')
            setSelectedIndex('');
        }
        //setting the category name
        else {
            setCategoryName(categoryName)
            setSelectedIndex(index);
        }
    }

    //filtering the items based on the selected category
    useEffect(() => {
        if (categoryName) {
            const tempItemsList = sortedByTitle.filter((item) => {
                return item.category === categoryName;
            })
            setItemsList(tempItemsList);
        }
        else {
            setItemsList(sortedByTitle);
        }
    }, [categoryName])
    
    let tempObj = {}
    data.favourite_categories.forEach((item) => {
        tempObj[item] = true;
    })
    data.items.forEach((item) => {
        tempObj[item.category] = true;
    })
    let categoryList = Object.keys(tempObj)
    
    return (
        <>
            <Grid container spacing={3}>
                <Grid item xs={6}>
                    <b>Category List</b>
                    {
                        categoryList && categoryList.map((item, index) => {
                            const setColor = data.favourite_categories.includes(item);
                            return <ListItem key={index} style={{ color: setColor ? 'blue' : '' }} selected={index === selectedIndex ? true : false} onClick={() => { handleCategoryClick(item, index) }}>
                                {item}
                            </ListItem>
                        }
                        )
                    }
                </Grid>
                <Grid item xs={6}>
                    <b> Item List</b>
                    {
                        itemsList && itemsList.map((item) =>
                            <ListItem key={item.id}>
                                {item.title}
                            </ListItem>
                        )
                    }
                </Grid>
            </Grid>
        </>
    )
}
export default Filters;
